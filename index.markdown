---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Dev/DevOps admin

Tuto stránku jsem vytvořil jako rozcestník/portfolio svých projektů a aktivit.

Kde co najít:
 - CV cz: [https://s3.eu-central-1.amazonaws.com/beranm-cv/latest/main-cz.pdf](https://s3.eu-central-1.amazonaws.com/beranm-cv/latest/main-cz.pdf)
 - CV en: [https://s3.eu-central-1.amazonaws.com/beranm-cv/latest/main-en.pdf](https://s3.eu-central-1.amazonaws.com/beranm-cv/latest/main-en.pdf)
 - GitHub: [https://github.com/beranm14](https://github.com/beranm14)
 - Gitlab: [https://gitlab.com/beranm](https://gitlab.com/beranm)
 - LinkedIn: [https://www.linkedin.com/in/martin-beránek](https://www.linkedin.com/in/martin-beránek)
 - Twitter: [https://twitter.com/beranm14](https://twitter.com/beranm14)
 - Facebook: [https://www.facebook.com/martin.beranek3](https://www.facebook.com/martin.beranek3)
 - Youtube: [https://www.youtube.com/channel/UCKHM8jPGIXedMNFAJHma-Qw](https://www.youtube.com/channel/UCKHM8jPGIXedMNFAJHma-Qw)
 - Email: [martin.beranek112@gmail.com](mailto: martin.beranek112@gmail.com)
 - Medium: [https://martin-beranek.medium.com/](https://martin-beranek.medium.com/)

## Fakturační údaje

| IČO:    | 09461451 |
|---------|----------|
| Adresa: | Praha 12, Modřany, Pertoldova 3340/37 |

